<?php

/**
 * Implementation of hook_node_info().
 */
function audio_audiofield_node_info() {
  $items = array(
    'podcast' => array(
      'name' => t('Podcast'),
      'module' => 'features',
      'description' => t('Podcast'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
