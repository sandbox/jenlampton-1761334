<?php
/**
 * @file
 * Update functions.
 */

/**
 * Implements hook_install().
 */
function audio_audiofield_install() {
  // Set an initial value for the schema version so we can run updates after install.
  drupal_set_installed_schema_version('audio_audiofield', 6000);
}

/**
 * Change node type to move off audio module.
 * See http://geeksandgod.com/tutorials/computers/cms/drupal/drupal-converting-audio-module-filefield-module
 */
function audio_audiofield_update_6001(&$sandbox) {
  $ret = array();
  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['current_nid'] = 0;
    $sandbox['max'] = db_result(db_query("SELECT COUNT(DISTINCT nid) FROM {node} WHERE type = 'audio'"));
  }

  // Update 30 nodes at a time.
  $limit = 30;
  $result = db_query_range("SELECT nid FROM {node} WHERE type = 'audio' ORDER BY nid ASC", $sandbox['current_nid'], $limit);

  while ($nid = db_result($result)) {
    // Load the node
    $node = node_load($nid);
    // Change the node type.
    $node->type = 'podcast';
    // Move the audio data into the audiofield.
    $node->field_podcast[0]['fid'] = $node->audio['file']->fid;
    $node->field_podcast[0]['uid'] = $node->audio['file']->uid;
    $node->field_podcast[0]['filename'] = $node->audio['file']->filename;
    $node->field_podcast[0]['filepath'] = $node->audio['file']->filepath;
    $node->field_podcast[0]['filemime'] = $node->audio['file']->filemime;
    $node->field_podcast[0]['filesize'] = $node->audio['file']->filesize;
    $node->field_podcast[0]['status'] = $node->audio['file']->status;
    $node->field_podcast[0]['timestamp'] = $node->audio['file']->timestamp;
    $node->field_podcast[0]['list'] = 0; // Disables filefield style display.
    $node->field_podcast[0]['data'] = NULL;
    $node->field_podcast[0]['nid'] = $node->nid;

    // Remove the audio data.
    unset($node->audio);
    unset($node->audio_tags);
    unset($node->url_play);
    unset($node->url_download);

    // Save the node.
    node_save($node);

    $sandbox['progress']++;
    $sandbox['current_nid'] = $node->nid;
  }

  $ret['#finished'] = empty($sandbox['max']) ? 1 : ($sandbox['progress'] / $sandbox['max']);

  return $ret;
}

